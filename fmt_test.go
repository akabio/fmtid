package fmtid

import (
	"testing"
	"unicode"

	"github.com/akabio/expect"
)

func TestFmtFoo(t *testing.T) {
	expect.Value(t, "formats", eval(t, "Foo", ASCII)).ToBe(map[string]string{
		"AsID":   "Foo",
		"AsId":   "Foo",
		"as-id":  "foo",
		"as_id":  "foo",
		"AS_ID":  "FOO",
		"asID":   "foo",
		"asId":   "foo",
		"asid":   "foo",
		"ASID":   "FOO",
		"AsIDs":  "Foos",
		"AsIds":  "Foos",
		"as-ids": "foos",
		"as_ids": "foos",
		"AS_IDS": "FOOS",
		"asIDs":  "foos",
		"asIds":  "foos",
		"asids":  "foos",
		"ASIDS":  "FOOS",
	})
}

func TestFmtGreekἄλφα(t *testing.T) {
	expect.Value(t, "formats", eval(t, "ἄλφα", Unicode)).ToBe(map[string]string{
		"AsID":   "Ἄλφα",
		"AsId":   "Ἄλφα",
		"as-id":  "ἄλφα",
		"as_id":  "ἄλφα",
		"AS_ID":  "ἌΛΦΑ",
		"asID":   "ἄλφα",
		"asId":   "ἄλφα",
		"asid":   "ἄλφα",
		"ASID":   "ἌΛΦΑ",
		"AsIDs":  "Ἄλφα",
		"AsIds":  "Ἄλφα",
		"as-ids": "ἄλφα",
		"as_ids": "ἄλφα",
		"AS_IDS": "ἌΛΦΑ",
		"asIDs":  "ἄλφα",
		"asIds":  "ἄλφα",
		"asids":  "ἄλφα",
		"ASIDS":  "ἌΛΦΑ",
	})
}

func TestFmtFooBar(t *testing.T) {
	expect.Value(t, "formats", eval(t, "FooBar", ASCII)).ToBe(map[string]string{
		"AsID":   "FooBar",
		"AsId":   "FooBar",
		"as-id":  "foo-bar",
		"as_id":  "foo_bar",
		"AS_ID":  "FOO_BAR",
		"asID":   "fooBar",
		"asId":   "fooBar",
		"asid":   "foobar",
		"ASID":   "FOOBAR",
		"AsIDs":  "FooBars",
		"AsIds":  "FooBars",
		"as-ids": "foo-bars",
		"as_ids": "foo_bars",
		"AS_IDS": "FOO_BARS",
		"asIDs":  "fooBars",
		"asIds":  "fooBars",
		"asids":  "foobars",
		"ASIDS":  "FOOBARS",
	})
}

func TestFmtFooIdBar(t *testing.T) {
	expect.Value(t, "formats", eval(t, "FooIdBar", ASCII)).ToBe(map[string]string{
		"AsID":   "FooIDBar",
		"AsId":   "FooIdBar",
		"as-id":  "foo-id-bar",
		"as_id":  "foo_id_bar",
		"AS_ID":  "FOO_ID_BAR",
		"asID":   "fooIDBar",
		"asId":   "fooIdBar",
		"asid":   "fooidbar",
		"ASID":   "FOOIDBAR",
		"AsIDs":  "FooIDBars",
		"AsIds":  "FooIdBars",
		"as-ids": "foo-id-bars",
		"as_ids": "foo_id_bars",
		"AS_IDS": "FOO_ID_BARS",
		"asIDs":  "fooIDBars",
		"asIds":  "fooIdBars",
		"asids":  "fooidbars",
		"ASIDS":  "FOOIDBARS",
	})
}

func TestFmtFooId(t *testing.T) {
	expect.Value(t, "formats", eval(t, "FooId", ASCII)).ToBe(map[string]string{
		"AsID":   "FooID",
		"AsId":   "FooId",
		"as-id":  "foo-id",
		"as_id":  "foo_id",
		"AS_ID":  "FOO_ID",
		"asID":   "fooID",
		"asId":   "fooId",
		"asid":   "fooid",
		"ASID":   "FOOID",
		"AsIDs":  "FooIDs",
		"AsIds":  "FooIds",
		"as-ids": "foo-ids",
		"as_ids": "foo_ids",
		"AS_IDS": "FOO_IDS",
		"asIDs":  "fooIDs",
		"asIds":  "fooIds",
		"asids":  "fooids",
		"ASIDS":  "FOOIDS",
	})
}

func TestCustomPlural(t *testing.T) {
	fmt := AddDefault(&Formatter{
		Plurals: map[string]string{"data": "datas"},
		IsFirst: unicode.IsUpper,
		IsOther: unicode.IsLower,
	})

	expect.Value(t, "formats", eval(t, "FooData", fmt)).ToBe(map[string]string{
		"AsID":   "FooData",
		"AsId":   "FooData",
		"as-id":  "foo-data",
		"as_id":  "foo_data",
		"AS_ID":  "FOO_DATA",
		"asID":   "fooData",
		"asId":   "fooData",
		"asid":   "foodata",
		"ASID":   "FOODATA",
		"AsIDs":  "FooDatas",
		"AsIds":  "FooDatas",
		"as-ids": "foo-datas",
		"as_ids": "foo_datas",
		"AS_IDS": "FOO_DATAS",
		"asIDs":  "fooDatas",
		"asIds":  "fooDatas",
		"asids":  "foodatas",
		"ASIDS":  "FOODATAS",
	})
}

func TestReadme(t *testing.T) {
	x, _ := ASCII.Fmt("AS_ID", "redHering")
	expect.Value(t, "id", x).ToBe("RED_HERING")
	x, _ = ASCII.Fmt("AS_IDS", "redHering")
	expect.Value(t, "id", x).ToBe("RED_HERINGS")
	x, _ = Unicode.Fmt("AS_IDS", "röterTαx")
	expect.Value(t, "id", x).ToBe("RÖTER_TΑXES")
}

func TestParseValidPatterns(t *testing.T) {
	r, _ := ASCII.parse("Foo")
	expect.Value(t, "parts", r).ToBe([]string{"foo"})

	r, _ = ASCII.parse("FooBar")
	expect.Value(t, "parts", r).ToBe([]string{"foo", "bar"})

	r, _ = ASCII.parse("FB01")
	expect.Value(t, "parts", r).ToBe([]string{"f", "b01"})

	r, _ = ASCII.parse("foo")
	expect.Value(t, "parts", r).ToBe([]string{"foo"})

	r, _ = ASCII.parse("fooBar")
	expect.Value(t, "parts", r).ToBe([]string{"foo", "bar"})

	r, _ = ASCII.parse("fB01")
	expect.Value(t, "parts", r).ToBe([]string{"f", "b01"})
}

func TestParseInvalidPatterns(t *testing.T) {
	_, err := ASCII.parse("")
	expect.Value(t, "error", err.Error()).ToBe("identifier must not be the empty string")

	_, err = ASCII.parse("Fooä")
	expect.Value(t, "error", err.Error()).ToBe("invalid character 'ä' in identifier 'Fooä'")

	_, err = ASCII.parse("-ff")
	expect.Value(t, "error", err.Error()).ToBe("invalid character '-' in identifier '-ff'")
}

func eval(t *testing.T, id string, f *Formatter) map[string]string {
	vals := map[string]string{}
	for k, f := range f.Formats {
		res, err := f(id)
		if err != nil {
			t.Fatal(err)
		}
		vals[k] = res
	}
	return vals
}

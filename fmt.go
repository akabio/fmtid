package fmtid

import (
	"errors"
	"fmt"
	"strings"
	"unicode"

	"github.com/jinzhu/inflection"
)

type Formatter struct {
	Acronyms       map[string]bool
	PluralAcronyms map[string]bool
	Plurals        map[string]string
	IsFirst        func(rune) bool
	IsOther        func(rune) bool
	Formats        map[string]func(string) (string, error)
}

var ASCII = AddDefault(&Formatter{
	Acronyms: map[string]bool{
		"id":   true,
		"http": true,
		"tcp":  true,
		"ip":   true,
	},
	PluralAcronyms: map[string]bool{
		"ids": true,
		"ips": true,
	},
	IsFirst: func(r rune) bool {
		return strings.ContainsRune("ABCDEFGHIJKLMNOPQRSTUVWXYZ", r)
	},
	IsOther: func(r rune) bool {
		return strings.ContainsRune("abcdefghijklmnopqrstuvwxyz0123456789", r)
	},
})

var Unicode = AddDefault(&Formatter{
	Acronyms: map[string]bool{
		"id":   true,
		"http": true,
		"tcp":  true,
		"ip":   true,
	},
	PluralAcronyms: map[string]bool{
		"ids": true,
		"ips": true,
	},
	IsFirst: unicode.IsUpper,
	IsOther: func(r rune) bool {
		return unicode.IsLower(r) || unicode.IsNumber(r)
	},
})

// Fmt will format the name according to the given format string.
func (f *Formatter) Fmt(format string, name string) (string, error) {
	ff, has := f.Formats[format]
	if !has {
		return "", fmt.Errorf("unknown format '%v'", format)
	}
	return ff(name)
}

// AddDefault adds default formats to the given formatter
// and returns it. Does not create a new instance.
func AddDefault(f *Formatter) *Formatter {
	if f.Formats == nil {
		f.Formats = map[string]func(string) (string, error){}
	}

	ta := map[string]func(string) (string, error){
		"as-id": f.makeParseFmt("-", func(i int, s string) string { return s }),
		"as_id": f.makeParseFmt("_", func(i int, s string) string { return s }),
		"AS_ID": f.makeParseFmt("_", func(i int, s string) string { return cap(s) }),
		"asId":  f.makeParseFmt("", func(i int, s string) string { return u(s, i > 0) }),
		"AsId":  f.makeParseFmt("", func(i int, s string) string { return u(s, true) }),
		"asID":  f.makeParseFmt("", func(i int, s string) string { return f.ug(s, i > 0) }),
		"AsID":  f.makeParseFmt("", func(i int, s string) string { return f.ug(s, true) }),
		"asid":  f.makeParseFmt("", func(i int, s string) string { return s }),
		"ASID":  f.makeParseFmt("", func(i int, s string) string { return cap(s) }),
	}

	for k, v := range ta {
		f.Formats[k] = v
		f.Formats[k+k[1:2]] = f.mkPlural(v)
	}
	return f
}

func (f *Formatter) parse(n string) ([]string, error) {
	if n == "" {
		return nil, errors.New("identifier must not be the empty string")
	}

	tokens := []string{}
	token := ""

	for i, r := range n {
		if i == 0 {
			if !f.IsFirst(r) && !f.IsOther(r) {
				return nil, fmt.Errorf("invalid character '%v' in identifier '%v'", string(r), n)
			}
			token = string(r)
			continue
		}
		if f.IsFirst(r) {
			tokens = append(tokens, strings.ToLower(token))
			token = string(r)
			continue
		}
		if f.IsOther(r) {
			token += string(r)
			continue
		}
		return nil, fmt.Errorf("invalid character '%v' in identifier '%v'", string(r), n)
	}
	tokens = append(tokens, strings.ToLower(token))

	return tokens, nil
}

func (f *Formatter) fmt(id []string, sep string, ff func(int, string) string) string {
	npts := []string{}
	for i, v := range id {
		npts = append(npts, ff(i, v))
	}
	return strings.Join(npts, sep)
}

func (f *Formatter) parseFmt(id string, sep string, ff func(int, string) string) (string, error) {
	idp, err := f.parse(id)
	if err != nil {
		return "", err
	}
	return f.fmt(idp, sep, ff), nil
}

func (f *Formatter) makeParseFmt(sep string, ff func(int, string) string) func(string) (string, error) {
	return func(id string) (string, error) {
		return f.parseFmt(id, sep, ff)
	}
}

func (f *Formatter) mkPlural(ff func(string) (string, error)) func(string) (string, error) {
	return func(s string) (string, error) {
		parts, err := f.parse(s)
		if err != nil {
			return "", err
		}

		last := parts[len(parts)-1]
		customPlural := false
		if f.Plurals != nil {
			plural, has := f.Plurals[last]
			if has {
				last = plural
				customPlural = true
			}
		}
		if !customPlural {
			last = inflection.Plural(parts[len(parts)-1])
		}

		parts[len(parts)-1] = last
		res := f.fmt(parts, "", func(i int, s string) string { return u(s, true) })
		return ff(res)
	}
}

func (f *Formatter) ug(i string, do bool) string {
	if do {
		if f.Acronyms[i] {
			return strings.ToUpper(i)
		}
		if f.PluralAcronyms[i] {
			rs := []rune(i)
			return strings.ToUpper(string(rs[:len(rs)-1])) + string(rs[len(rs)-1:])
		}
		rs := []rune(i)
		return strings.ToUpper(string(rs[0:1])) + string(rs[1:])
	}
	return i
}

func u(i string, do bool) string {
	if do {
		rs := []rune(i)
		return strings.ToUpper(string(rs[0:1])) + string(rs[1:])
	}
	return i
}

var cap = strings.ToUpper

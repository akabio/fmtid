module gitlab.com/akabio/fmtid

go 1.19

require (
	github.com/akabio/expect v0.10.2
	github.com/jinzhu/inflection v1.0.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/ghodss/yaml v1.0.0 // indirect
	github.com/sergi/go-diff v1.2.0 // indirect
	golang.org/x/exp v0.0.0-20230801115018-d63ba01acd4b // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)

# fmtid

formats identifiers into different name schemas like

- camelCase
- kebab-case
- snake_case

As input it takes an identifier in lower or upper camelCase with acronyms
first upper and rest lowercase. The canonical form is UpperCamelCase.

## purpose

It's intended purpose is to generate code for different data and programming languages.

The Identifier `customerId` is in go expected to be `customerID` or `CustomerID` whereas in json it's common to use `customerId` whereas in many languages as a constant it would be `CUSTOMER_ID`.

## api

The `Fmt` function is available for ASCII and Unicode and can be used like:

```go
import "gitlab.com/akabio/fmtid"

...

fmt.Println(fmtid.ASCII.Fmt("AS_ID", "redHering")) // -> RED_HERING
fmt.Println(fmtid.ASCII.Fmt("AS_IDS", "redHering")) // -> RED_HERINGS

fmt.Println(fmtid.Unicode.Fmt("AS_IDS", "röterTαx")) // -> RÖTER_TΑXES

```

The functions take a format string and the input string in camel case and turn it into it's expected output. They are also available inside a map with the format string as key and a function as value. The format strings use the name `asId` in the target format. The default formats are:

- AsID -> FooBar
- AsId -> FooBar
- as-id -> foo-bar
- as_id -> foo_bar
- AS_ID -> FOO_BAR
- asID -> fooBar
- asId -> fooBar
- asid -> foobar
- ASID -> FOOBAR
- AsIDs -> FooBars
- AsIds -> FooBars
- as-ids -> foo-bars
- as_ids -> foo_bars
- AS_IDS -> FOO_BARS
- asIDs -> fooBars
- asIds -> fooBars
- asids -> foobars
- ASIDS -> FOOBARS

